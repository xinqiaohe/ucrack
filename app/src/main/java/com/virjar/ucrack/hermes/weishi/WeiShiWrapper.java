package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.AsyncResult;
import com.virjar.hermes.hermesagent.hermes_api.MultiActionWrapper;
import com.virjar.xposed_extention.ClassLoadMonitor;
import com.virjar.xposed_extention.SingletonXC_MethodHook;

import de.robv.android.xposed.XposedHelpers;

public class WeiShiWrapper extends MultiActionWrapper {
    @Override
    public void onXposedHotLoad() {

        final SingletonXC_MethodHook paramInterceptorHook = new SingletonXC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                Object request = param.args[0];
                final Object response = param.args[1];

                AsyncResult asyncResult = WeishiUtil.requestMap.remove(request);
                if (asyncResult == null) {
                    return;
                }
                asyncResult.notifyDataArrival(XposedHelpers.callMethod(response, "d"));
                param.setResult(true);
            }
        };


        //该方法为网络响应回调，数据解码之后的第一个回调，
        // 其中com.tencent.oscar.utils.network.d为请求封装，com.tencent.oscar.utils.network.e为响应封装
        // com.tencent.oscar.utils.network.e#d 里面包裹了响应数据对象，为一个com.qq.taf.jce.JceStruct类型，二进制编码报文
        //com.tencent.oscar.utils.network.j#a(com.tencent.oscar.utils.network.d, com.tencent.oscar.utils.network.e)
        ClassLoadMonitor.addClassLoadMonitor("com.tencent.oscar.utils.network.j", new ClassLoadMonitor.OnClassLoader() {
            @Override
            public void onClassLoad(Class clazz) {
                if (!(android.os.HandlerThread.class.isAssignableFrom(clazz))) {
                    return;
                }

                XposedHelpers.findAndHookMethod(clazz, "a",
                        "com.tencent.oscar.utils.network.d", "com.tencent.oscar.utils.network.e", paramInterceptorHook);
            }
        });

        ClassLoadMonitor.addClassLoadMonitor("com.tencent.oscar.utils.network.h", new ClassLoadMonitor.OnClassLoader() {
            @Override
            public void onClassLoad(Class clazz) {
                if (!(android.os.HandlerThread.class.isAssignableFrom(clazz))) {
                    return;
                }

                XposedHelpers.findAndHookMethod(clazz, "a",
                        "com.tencent.oscar.utils.network.d", "com.tencent.oscar.utils.network.e", paramInterceptorHook);
            }
        });

    }
}
